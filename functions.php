<?php

function xdedalex_enqueue() {
	wp_enqueue_style( 'davledoux-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'dedale', get_stylesheet_directory_uri() . '/style.css', array(  ), wp_get_theme()->get('Version') );
}

add_action( 'wp_enqueue_scripts', 'xdedalex_enqueue' );
